package ufps.util;

import ufps.util.colecciones_seed.ArbolBinarioDiccionario;

public class ArbolDigital {

    private String[] lineas;
    private ArbolBinarioDiccionario<Letra> arbol;

    public ArbolDigital(String[] lineas) {
        this.lineas = lineas;
        llenarArbol();
    }

    private void llenarArbol() {
        arbol = new ArbolBinarioDiccionario<>((Letra) new Letra('@', false));

        String linea;
        for (short i = 0; i < lineas.length; i++) {
            linea = lineas[i].toLowerCase();

            if (i < lineas.length - 1) {
                linea += "/";
            }

            arbol.llenarArbol(linea, i);
        }
    }

    public String[] getLineas() {
        return lineas;
    }

    public void setLineas(String[] lineas) {
        this.lineas = lineas;
    }

    public ArbolBinarioDiccionario<Letra> getArbol() {
        return arbol;
    }

    public void setArbol(ArbolBinarioDiccionario<Letra> arbol) {
        this.arbol = arbol;
    }

    public int getCoincidencias(String palabra) {

        return this.arbol.buscarCoincidencias(palabra);
    }

}
