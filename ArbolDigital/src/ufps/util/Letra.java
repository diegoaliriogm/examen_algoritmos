package ufps.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Letra {

    public static final char LETRA_RAIZ = '@';
    public static final char LETRA_NODO_IZQ = '0';
    public static final char LETRA_NODO_DER = '1';
    public static final char LETRA_NODO_VACIO = '$';

    private List<short[]> posiciones;
    private Character letra;
    private boolean esDerecho;
    private boolean nodoColor;

    public Letra(Character letra, boolean esDerecho, short posicionX, short posicionY) {
        posiciones = new ArrayList<>();
        posiciones.add(new short[]{posicionX, posicionY, 0});
        this.esDerecho = esDerecho;
        this.letra = letra;
    }

    public Letra(Character letra, boolean esDerecho) {
        this.letra = letra;
        this.esDerecho = esDerecho;
    }

    public List<short[]> getPosiciones() {
        return posiciones;
    }

    public void setPosiciones(List<short[]> posiciones) {
        this.posiciones = posiciones;
    }

    public boolean isEsDerecho() {
        return esDerecho;
    }

    public void setEsDerecho(boolean esDerecho) {
        this.esDerecho = esDerecho;
    }

    public Character getLetra() {
        return letra;
    }

    public boolean isNodoColor() {
        return nodoColor;
    }

    public void setNodoColor(boolean nodoColor) {
        this.nodoColor = nodoColor;
    }

    public void setLetra(Character letra) {
        this.letra = letra;
    }

    public void addPosicion(short posicionX, short posicionY) {
        posiciones.add(new short[]{posicionX, posicionY, 0});
    }

    public void limpiarCoincidencias() {
        if (posiciones != null) {
            for (short[] posicion : posiciones) {
                posicion[2] = 0;
            }
        }
        this.nodoColor = false;
    }

    public boolean letraCoincide() {

        if (posiciones != null) {
            for (short[] posicion : posiciones) {
                if (posicion[2] != 0) {
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public String toString() {

        // RAIZ
        if (this.letra == LETRA_RAIZ) {
            return "   " + LETRA_RAIZ;
        }
        // NODOS VACIOS
        if (this.letra == LETRA_NODO_VACIO) {
            return "    " + (this.esDerecho ? LETRA_NODO_DER : LETRA_NODO_IZQ);
        }
        // LETRA DEL DICCIONARIO
        String texto = "    " + (this.esDerecho ? LETRA_NODO_DER : LETRA_NODO_IZQ) + "\n";

        switch (this.letra) {
            case '/':
                texto += "    \\n\n";
                break;
            case ' ':
                texto += "  esp\n";
                break;
            case '#':
                texto += "   ch\n";
                break;
            default:
                texto += "    " + letra.toString() + "\n";
        }

        for (int i = 0; i < posiciones.size(); i++) {
            short[] pos = posiciones.get(i);
            texto += (pos[2] != 0 ? "> " : "") + "[" + pos[0] + "," + pos[1] + "]";
            if (i < posiciones.size() - 1) {
                texto += "\n";
            }
        }

        return texto;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.letra);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Letra other = (Letra) obj;
        return Objects.equals(this.letra, other.letra);
    }

}
